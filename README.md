Starter Pack for Laravel 8 with ADMINTY Template. 
Modify with your own needs.

## git config --global user.name "I Gede Adi Surya Eka Pramana Putra"
## git config --global user.email "gedeeinstein@gmail.com"

# Create a new repository
## git clone https://gitlab.com/gedeeinstein/laravel-crud-generator-starter-pack.git
## cd odoo14
## touch README.md
## git add README.md
## git commit -m "add README"
## git push -u origin master

# Push an existing folder
## cd existing_folder
## git init
## git remote add origin https://gitlab.com/gedeeinstein/laravel-crud-generator-starter-pack.git
## git add .
## git commit -m "Initial commit"
## git push -u origin master

# Push an existing Git repository
cd existing_repo
## git remote rename origin old-origin
## git remote add origin https://gitlab.com/gedeeinstein/laravel-crud-generator-starter-pack.git
## git push -u origin --all
## git push -u origin --tags