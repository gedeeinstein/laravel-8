<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\TemplateSurat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TemplateSuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new \stdClass;  
        $data->page_title = "Templates Surat";
        $data->templates = TemplateSurat::all()->load('user');
        $data->users = User::all();
        return view('templates.index', (array) $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = Auth::user()->id;
        $data = new \stdClass;
        $data->page_title = "Buat Template";
        $data->user = $this->user;
        // return $data->user;
        return view('templates.create', (array) $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required', 'string', 'max:255',
            'desc' => 'required', 'string', 'max:255',
            'user_id' => 'required',
        ]);

        $template = TemplateSurat::create($request->all());
        // return $template;
        $template->save();
        return redirect()->route('templates.index')->with('success', 'Template Surat created successfully.');
        // $this->validate($request, $rules, $errorMessage);
      
        // Customer::create([
        //    'name' => $request->name,
        //    'slug' => \Str::slug($request->name),
        //    'email' => strtolower($request->email)
        // ]);
  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TemplateSurat  $templateSurat
     * @return \Illuminate\Http\Response
     */
    public function show(TemplateSurat $templateSurat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TemplateSurat  $templateSurat
     * @return \Illuminate\Http\Response
     */
    public function edit(TemplateSurat $templateSurat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TemplateSurat  $templateSurat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TemplateSurat $templateSurat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TemplateSurat  $templateSurat
     * @return \Illuminate\Http\Response
     */
    public function destroy(TemplateSurat $templateSurat)
    {
        //
    }



    public function template01()
    {
        // $data = new \stdClass;  
        // $data->page_title = "Templates Surat";
        // $data->users = User::all();
        // $data->templates = TemplateSurat::with('user');

        // // return (array) $data;

        // return view('templates.index', (array) $data );
        // // return route('template.test', (array) $data );
        // // return view( 'templates.index', (array) $data );
        return view('layouts.page');
    }
}
