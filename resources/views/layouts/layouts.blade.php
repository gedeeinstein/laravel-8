<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    @include('layouts.partials.headcss')

    @yield('css')

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <!-- Pre-loader start -->
    @include('layouts.partials.preload')
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            @include('layouts.partials.navheader')

            <!-- Sidebar chat start -->
            @include('layouts.partials.headbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar INNER CHAT Ww/NAV --}}
                    @include('layouts.partials.sidebar')
                    
                    {{-- START MAIN CONTENT WILL DISPLAYED HERE --}}

                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            @yield('main_content')
                                        </div>
                                    </div>
                                </div>

                                <div id="styleSelector">

                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END MAIN CONTENT --}}
                </div>
            </div>
        </div>
    </div>

    <!-- Required Jquery -->
    @include('layouts.partials.script')
    <script src="{{ asset('js/app.js') }}"></script>
   
    @yield('javascript')

</body>

</html>
