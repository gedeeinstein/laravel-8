    <link rel="icon" href="{{ asset('templates/assets/images/favicon.ico') }}" type=image/x-icon>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/bower_components/bootstrap/css/bootstrap.min.css') }}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/assets/icon/feather/css/feather.css') }}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/bower_components/select2/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/assets/css/jquery.mCustomScrollbar.css') }}">
    