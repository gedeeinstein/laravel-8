import pathlib
import dropbox
import re

# file yang akan diupload di PC kita
folder = pathlib.Path(".")    # file berada di current folder
filename = "fotosaya.jpg"         # nama file
filepath = folder / filename  # path ke file

# lokasi target di Dropbox
target = "/"   # folder target
targetfile = target + filename   # path target di dropbox

# Membuat objek dropbox menggunakan key / token dari akun dropbox
d = dropbox.Dropbox('PASTE-KAN TOKEN DROPBOX DI SINI')

# open file dan upload
with filepath.open("rb") as f:
   meta = d.files_upload(f.read(), targetfile, mode=dropbox.files.WriteMode("overwrite"))

# membuat link sharing file
link = d.sharing_create_shared_link(targetfile)

# url yang bisa dishare
#url = link.url

# file bisa langsung dibuka di browser dengan mengganti ?dl=0 dengan ?raw=1
#view_url = re.sub(r"\?dl\=0", "?raw=1", url)
#print (view_url)