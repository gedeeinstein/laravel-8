
<x-app-layout>
    <x-slot name="header">
        <a href="{{ route('templates.create') }}" type="button" class="btn btn-info">Add New</a>
    </x-slot>
    
    <div class="card bg-light">
        <div class="card-body">
            <button></button>
            <div class="table-responsive max-w-7xl mx-auto sm:px-6 lg:px-8">
                <table class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-center" data-id="id_template">ID</th>
                            <th class="text-center" data-id="template_name">Template Name</th>
                            <th class="text-center" data-id="template_description">Desc</th>
                            <th class="text-center" data-id="creation_date">Created Date</th>
                            <th class="text-center" data-id="action"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($templates as $template)
                            <tr>
                                <th data-id="id_template">{{ $template->id }}</th>
                                <th data-id="template_name"> {{ $template->name }} </th>
                                <th data-id="template_description">{{ $template->desc }}</th>
                                <th data-id="creation_date">{{ \Carbon\Carbon::parse($template->created_at)->diffForHumans() }}</th>
                                <th data-id="action" class="text-center">
                                    <a href="{{ route('templates.edit',$template->id ) }}" class="btn btn-danger btn-sm" >
                                        Edit
                                    </a> 
                                    <a href="{{ route('templates.show',$template->id ) }}" class="btn btn-info btn-sm" >
                                        View
                                    </a> 
                                    <a href="{{ route('templates.destroy',$template->id ) }}" class="btn btn-warning btn-sm" >
                                        Delete
                                    </a> 
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
