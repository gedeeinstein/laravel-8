
<x-app-layout>
    <x-slot name="header">
        <a href="{{ route('templates.index') }}" type="button" class="btn btn-success">Back</a>
    </x-slot>
    
    <div class="card bg-light">
        <div class="card-body">
            <button></button>
            <div class="table-responsive max-w-7xl mx-auto sm:px-6 lg:px-8">
                <form action="{{route('templates.store')}}" method="post">
                    @csrf
                    @method('POST')
                    <div class="form-group">
                        <label for="name">Nama Templates</label>
                        <input type="text" class="form-control" id="name" name="name">
                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{ $user }}">
                    </div>
                    <div class="form-group">
                        <label for="desc">Deskripsi</label>
                        <textarea class="form-control" name="desc" id="desc" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>

                {{-- <table class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-center" data-id="id_template">ID</th>
                            <th class="text-center" data-id="template_name">Template Name</th>
                            <th class="text-center" data-id="template_description">Desc</th>
                            <th class="text-center" data-id="creation_date">Created Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($templates as $template)
                            <tr>
                                <th data-id="id_template">{{ $template->id }}</th>
                                <th data-id="template_name"> {{ $template->name }} </th>
                                <th data-id="template_description">{{ $template->desc }}</th>
                                <th data-id="creation_date">{{ $template->created_at }}</th>
                            </tr>
                        @endforeach
                    </tbody>
                </table> --}}
            </div>
        </div>
    </div>
</x-app-layout>
